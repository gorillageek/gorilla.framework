﻿using Gorilla.Utilities.Cripto;
using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace Gorilla.Utilities
{
    public static class ExtensionMethodsClass
    {
        /// <summary>
        /// Retorna a String Encriptada
        /// </summary>
        [DebuggerStepThrough]
        public static string Encript(this object valorEncriptar, string key = null)
        {
            if (valorEncriptar == null)
            {
                return null;
            }

            string retorno = null;
            using (var cripto = new Crypt(key))
            {
                retorno = cripto.Encrypt(valorEncriptar.ToString());
            }


            return retorno;
        }

        /// <summary>
        /// Retorna a String Decriptada
        /// </summary>
        [DebuggerStepThrough]
        public static string Decript(this string valorDecriptar, string key = null)
        {
            if (string.IsNullOrWhiteSpace(valorDecriptar))
            {
                return null;
            }

            string retorno = null;
            using (var cripto = new Crypt(key))
            {
                retorno = cripto.Decrypt(valorDecriptar);
            }

            return retorno;
        }

        /// <summary>
        /// Retorna o SHA-1 do texto passado
        /// </summary>
        [DebuggerStepThrough]
        public static string ToSHA1(this string valor)
        {
            var buffer = Encoding.UTF8.GetBytes(valor);
            var cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
            var hash = BitConverter.ToString(cryptoTransformSHA1.ComputeHash(buffer));
            return hash.Replace("-", "");
        }

        /// <summary>
        /// Retorna o MD5 do texto passado
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        [DebuggerStepThrough]
        public static string ToMD5(this string valor)
        {
            if (string.IsNullOrWhiteSpace(valor))
            {
                return string.Empty;
            }

            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(valor);
            var hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            foreach (var t in hash)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString().ToLower();
        }
    }
}
