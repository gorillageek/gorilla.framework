﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gorilla.Utilities.Attributes
{
    /// <summary>
    /// Attr to ignore props on LoadFrom
    /// </summary>
    public class IgnorableProperty : Attribute { }
}
