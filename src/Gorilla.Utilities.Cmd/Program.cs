﻿using System;

namespace Gorilla.Utilities.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Begin...");

            DateTime? date = null;
            Console.WriteLine(date.ToShortDateString());
            Console.WriteLine(date.ToString());
            Console.WriteLine(date.ToString("dd/MM"));

            date = DateTime.Now;
            Console.WriteLine(date.ToShortDateString());
            Console.WriteLine(date.ToString());
            Console.WriteLine(date.ToString("dd/MM"));
        }
    }
}
