﻿using Gorilla.Utilities.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gorilla.Utilities.Test.Models
{
    public class LoadFromOrigem
    {
        public LoadFromOrigem()
        {
            this.Ignore = "a";

            this.String = "a";
            this.StringNull = null;

            this.Int = 1;
            this.IntNullable = 1;
            this.IntNull = null;

            this.Datetime = DateTime.Now;
            this.DatetimeNullable = DateTime.Now;
            this.DatetimeNull = null;

            this.Float = 1;
            this.FloatNullable = 1;
            this.FloatNull = null;

            this.Decimal = 1;
            this.DecimalNullable = 1;
            this.DecimalNull = null;

            this.Short = 1;
            this.ShortNullable = 1;
            this.ShortNull = null;

            this.Double = 1;
            this.DoubleNullable = 1;
            this.DoubleNull = null;

            this.Byte = 1;
            this.ByteNullable = 1;
            this.ByteNull = null;

            this.Bool = true;
            this.BoolNullable = true;
            this.BoolNull = null;

            this.Char = 'a';
            this.CharNullable = 'a';
            this.CharNull = null;

            this.Long = 1;
            this.LongNullable = 1;
            this.LongNull = null;
        }

        [IgnorableProperty]
        public string Ignore { get; set; }

        public string String { get; set; }

        public string StringNull { get; set; }

        public int Int { get; set; }

        public int? IntNullable { get; set; }

        public int? IntNull { get; set; }

        public DateTime Datetime { get; set; }

        public DateTime? DatetimeNullable { get; set; }

        public DateTime? DatetimeNull { get; set; }

        public float Float { get; set; }

        public float? FloatNullable { get; set; }

        public float? FloatNull { get; set; }

        public decimal Decimal { get; set; }

        public decimal? DecimalNullable { get; set; }

        public decimal? DecimalNull { get; set; }

        public short Short { get; set; }

        public short? ShortNullable { get; set; }

        public short? ShortNull { get; set; }

        public double Double { get; set; }

        public double? DoubleNullable { get; set; }

        public double? DoubleNull { get; set; }

        public byte Byte { get; set; }

        public byte? ByteNullable { get; set; }

        public byte? ByteNull { get; set; }

        public bool Bool { get; set; }

        public bool? BoolNullable { get; set; }

        public bool? BoolNull { get; set; }

        public char Char { get; set; }

        public char? CharNullable { get; set; }

        public char? CharNull { get; set; }

        public long Long { get; set; }

        public long? LongNullable { get; set; }

        public long? LongNull { get; set; }
    }
}
