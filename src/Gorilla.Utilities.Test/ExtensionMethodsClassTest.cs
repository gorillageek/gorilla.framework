﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gorilla.Utilities.Test
{
    [TestClass]
    public class ExtensionMethodsClassTest
    {
        [TestMethod]
        public void OnlyNumbersTest()
        {
            var validator = new Validador();

            string alfa = "123456gb";
            string number = alfa.OnlyNumbers();

            int.Parse(number);
        }

        [TestMethod]
        public void LoadFromTest()
        {
            var origem = new Models.LoadFromOrigem();
            var destino = new Models.LoadFromDestino();

            destino.LoadFrom(origem);

            #region Verificaçoes

            Assert.AreEqual(origem.Bool, destino.Bool);
            Assert.AreEqual(origem.BoolNull, destino.BoolNull);
            Assert.AreEqual(origem.BoolNullable, destino.BoolNullable);
            Assert.AreEqual(origem.Byte, destino.Byte);
            Assert.AreEqual(origem.ByteNull, destino.ByteNull);
            Assert.AreEqual(origem.ByteNullable, destino.ByteNullable);
            Assert.AreEqual(origem.Char, destino.Char);
            Assert.AreEqual(origem.CharNull, destino.CharNull);
            Assert.AreEqual(origem.CharNullable, destino.CharNullable);
            Assert.AreEqual(origem.Datetime, destino.Datetime);
            Assert.AreEqual(origem.DatetimeNull, destino.DatetimeNull);
            Assert.AreEqual(origem.DatetimeNullable, destino.DatetimeNullable);
            Assert.AreEqual(origem.Decimal, destino.Decimal);
            Assert.AreEqual(origem.DecimalNull, destino.DecimalNull);
            Assert.AreEqual(origem.DecimalNullable, destino.DecimalNullable);
            Assert.AreEqual(origem.Double, destino.Double);
            Assert.AreEqual(origem.DoubleNull, destino.DoubleNull);
            Assert.AreEqual(origem.DoubleNullable, destino.DoubleNullable);
            Assert.AreEqual(origem.Float, destino.Float);
            Assert.AreEqual(origem.FloatNull, destino.FloatNull);
            Assert.AreEqual(origem.FloatNullable, destino.FloatNullable);
            Assert.AreEqual(origem.Int, destino.Int);
            Assert.AreEqual(origem.IntNull, destino.IntNull);
            Assert.AreEqual(origem.IntNullable, destino.IntNullable);
            Assert.AreEqual(origem.Long, destino.Long);
            Assert.AreEqual(origem.LongNull, destino.LongNull);
            Assert.AreEqual(origem.LongNullable, destino.LongNullable);
            Assert.AreEqual(origem.Short, destino.Short);
            Assert.AreEqual(origem.ShortNull, destino.ShortNull);
            Assert.AreEqual(origem.ShortNullable, destino.ShortNullable);
            Assert.AreEqual(origem.String, destino.String);
            Assert.AreEqual(origem.StringNull, destino.StringNull);

            Assert.AreNotEqual(origem.Ignore, destino.Ignore);

            #endregion
        }
    }
}
