using Android.Content;
using System;

namespace Gorilla.Utilities.Droid
{
    /// <summary>
    /// Gerenciador do shared preferences
    /// </summary>
    public class SharedPreferenceManager : IDisposable
    {
        private ISharedPreferences _preferences;

        private ISharedPreferencesEditor _preferencesEditor;
        private ISharedPreferencesEditor preferencesEditor
        {
            get
            {
                if (_preferencesEditor == null)
                {
                    _preferencesEditor = _preferences.Edit();
                }

                return _preferencesEditor;
            }
        }

        public SharedPreferenceManager(Context context, string preferences)
        {
            _preferences = context.GetSharedPreferences(preferences, FileCreationMode.Private);
        }

        /// <summary>
        /// Obtem o valor do key
        /// </summary>
        /// <param name="context"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetString(string key)
        {
            return _preferences.GetString(key, string.Empty);
        }

        /// <summary>
        /// Salva o valor e referencia a key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void PutString(string key, string value)
        {
            preferencesEditor.PutString(key, value);
        }

        /// <summary>
        /// Remove a key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            preferencesEditor.Remove(key);
        }

        public void Dispose()
        {
            if (_preferencesEditor != null)
            {
                preferencesEditor.Apply();

                _preferencesEditor.Dispose();
                _preferencesEditor = null;
            }

            _preferences.Dispose();
            _preferences = null;
        }
    }
}