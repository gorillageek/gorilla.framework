using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Gorilla.Utilities.Droid.Fragments
{
    /// <summary>
    /// Classe com o conteudo da tab do PagerSlidingTabStrip - https://github.com/jamesmontemagno/PagerSlidingTabStrip-for-Xamarin.Android
    /// </summary>
    public class TabFragmentContent
    {
        public string Title { get; set; }

        public int LayoutResource { get; set; }

        public delegate void CreateViewCallback(View view);

        /// <summary>
        /// Respons�vel pelo bind dos eventos, n�o obrigatorio
        /// </summary>
        public CreateViewCallback Callback { get; set; }
    }
}