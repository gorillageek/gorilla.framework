using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Gorilla.Utilities.Droid.Fragments
{
    /// <summary>
    /// Fragment que controla a tab recorrente do PagerSlidingTabStrip - https://github.com/jamesmontemagno/PagerSlidingTabStrip-for-Xamarin.Android
    /// </summary>
    public class TabFragment : Android.Support.V4.App.Fragment
    {
        public TabFragmentContent _content;

        /// <summary>
        /// Cria um nova instancia com o conteudo da tab
        /// </summary>
        /// <param name="tab"></param>
        /// <returns></returns>
        public static TabFragment NewInstance(TabFragmentContent tab)
        {
            var instance = new TabFragment();
            instance._content = tab;

            return instance;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }


        /// <summary>
        /// Inflate o layout passado e chama o callback para os binds
        /// </summary>
        /// <param name="inflater"></param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns></returns>
        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(_content.LayoutResource, container, false);

            if (_content.Callback != null)
            {
                _content.Callback(view);
            }

            return view;
        }
    }
}