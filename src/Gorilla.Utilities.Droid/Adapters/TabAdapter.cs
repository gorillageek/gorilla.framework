using Android.Support.V4.App;
using Gorilla.Utilities.Droid.Fragments;
using System.Collections.Generic;

namespace Gorilla.Utilities.Droid.Adapters
{
    /// <summary>
    /// Tab Adapter para o PagerSlidingTabStrip - https://github.com/jamesmontemagno/PagerSlidingTabStrip-for-Xamarin.Android
    /// </summary>
    public class TabAdapter : FragmentPagerAdapter
    {
        private List<TabFragmentContent> _tabs;

        /// <summary>
        /// Titutlos
        /// </summary>
        private List<string> Titles;


        public TabAdapter(Android.Support.V4.App.FragmentManager fm, List<TabFragmentContent> tabs)
            : base(fm)
        {
            _tabs = tabs;

            Titles = new List<string>();
            tabs.ForEach(x => { Titles.Add(x.Title); });
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(Titles[position]);
        }

        public override int Count
        {
            get { return Titles.Count; }
        }

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            return TabFragment.NewInstance(_tabs[position]);
        }
    }
}