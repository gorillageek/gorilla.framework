using Android.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gorilla.Utilities.Droid.Adapters
{
    /// <summary>
    /// Base adapter with null validation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EmptyBaseAdapter<T> : BaseAdapter<T>
    {
        private bool _nulo = false;
        private string _defaultMessage;

        public Activity context { get; set; }

        public string emptyMessage { get; set; }

        public List<T> items { get; set; }

        public EmptyBaseAdapter(Activity context, List<T> items)
        {
            this.context = context;
            this.items = items ?? new List<T>();
            this._nulo = this.items.Any() == false;
            this._defaultMessage = context.Resources.GetString(Resource.String.EmptyListViewMessage);

            if (IsNulo)
            {
                this.items.Add((T)Activator.CreateInstance(typeof(T)));
            }
        }

        public override T this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count(); }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override bool IsEnabled(int position)
        {
            return false;
        }

        public bool IsNulo
        {
            get { return _nulo; }
        }

        /// <summary>
        /// Set the view if the items is null
        /// </summary>
        /// <param name="position"></param>
        /// <param name="convertView"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (_nulo)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.EmptyListView, null);
                convertView.FindViewById<TextView>(Resource.Id.emptyListViewText).Text = emptyMessage ?? _defaultMessage;
            }

            return convertView;
        }
    }
}