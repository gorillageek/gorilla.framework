using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;
using Gorilla.Utilities.Droid.Enumns;
using Android.Views.InputMethods;
using Android.Views.Animations;

namespace Gorilla.Utilities.Droid
{
    /// <summary>
    /// Classe de fun�oes gerais
    /// </summary>
    public class Helper
    {
        /// <summary>
        /// Verifica a conexao do celular
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static enConnectivity CheckNetwork(Activity context)
        {
            var connectivityManager = (ConnectivityManager)context.GetSystemService(Activity.ConnectivityService);
            var activeConnection = connectivityManager.ActiveNetworkInfo;


            if (activeConnection == null || activeConnection.IsConnected == false)
            {
                return enConnectivity.None;
            }

            switch (activeConnection.Type)
            {
                case ConnectivityType.Mobile:
                case ConnectivityType.MobileDun:
                case ConnectivityType.MobileHipri:
                case ConnectivityType.MobileMms:
                case ConnectivityType.MobileSupl:
                    return enConnectivity.Mobile;
                case ConnectivityType.Wifi:
                    return enConnectivity.Wifi;
                default:
                    return enConnectivity.Mobile;
            }
        }

        /// <summary>
        /// Esconde o keyboard
        /// </summary>
        public static void HideKeyboard(Activity context)
        {
            var imm = (InputMethodManager)context.GetSystemService(Activity.InputMethodService);
            imm.HideSoftInputFromWindow(context.CurrentFocus.WindowToken, HideSoftInputFlags.None);

            context.CurrentFocus.ClearFocus();
        }

        /// <summary>
        /// PerformClick no button quando o EditorAction for chamado
        /// </summary>
        /// <param name="field"></param>
        /// <param name="button"></param>
        public static void EditorActionPerformClick(EditText field, Button button)
        {
            field.EditorAction += (object sender, TextView.EditorActionEventArgs e) =>
            {
                if (e.ActionId == global::Android.Views.InputMethods.ImeAction.Done)
                {
                    button.PerformClick();
                }
            };
        }
    }
}