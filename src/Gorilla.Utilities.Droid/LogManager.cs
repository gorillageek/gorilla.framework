using Gorilla.Utilities.Droid.Enumns;
using System;

namespace Gorilla.Utilities.Droid
{
    /// <summary>
    /// Classe para gerenciar o sistema de log do android
    /// </summary>
    public class LogManager
    {
        private string LogKey;

        public LogManager(string logKey)
        {
            this.LogKey = logKey;
        }

        /// <summary>
        /// Adiciona um registro no log do android
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="exception"></param>
        /// <param name="title"></param>
        public void Add(enLog tipo, Exception exception, string title = null)
        {
            Add(tipo, exception.InnerException != null ? exception.InnerException.ToString() : exception.ToString(), title);
        }

        /// <summary>
        /// Adiciona um registro no log do android
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="message"></param>
        public void Add(enLog tipo, string message, string title = null)
        {
            if (title != null)
            {
                message = string.Format("{0}: {1}", title, message);
            }

            switch (tipo)
            {
                case enLog.Warning:
                    Android.Util.Log.Warn(LogKey, message);
                    break;
                case enLog.Error:
                    Android.Util.Log.Error(LogKey, message);
                    break;
                default:
                    Android.Util.Log.Info(LogKey, message);
                    break;
            }
        }
    }
}